from django.urls import resolve
from django.test import TestCase

from .views import HeroesView,HeroCloudView, HeroSunfloweyView, HeroJesterView

# Create your tests here.
class HeroesPageTest(TestCase):
    def test_heroes_page_returns_correct_html(self):
        response = self.client.get('/heroes')
        self.assertTemplateUsed(response, 'detail_heroes.html')
        
class HeroCloudPageTest(TestCase):
    def test_cloud_page_returns_correct_html(self):
        response = self.client.get('/hero/cloud')
        self.assertTemplateUsed(response, 'detail_cloud.html')

class HeroSunfloweyPageTest(TestCase):
    def test_sunflowey_page_returns_correct_html(self):
        response = self.client.get('/hero/sunflowey')
        self.assertTemplateUsed(response, 'detail_sunflowey.html')

class HeroJesterPageTest(TestCase):
    def test_jester_page_returns_correct_html(self):
        response = self.client.get('/hero/jester')
        self.assertTemplateUsed(response, 'detail_jester.html')

