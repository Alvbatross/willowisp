from django.views.generic.base import TemplateView

# Create your views here.
class HeroesView(TemplateView):
    template_name = 'detail_heroes.html'

class HeroCloudView(TemplateView):
    template_name = 'detail_cloud.html'

class HeroSunfloweyView(TemplateView):
    template_name = 'detail_sunflowey.html'

class HeroJesterView(TemplateView):
    template_name = 'detail_jester.html'
