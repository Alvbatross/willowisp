from django.conf.urls import url

from .views import HeroesView,HeroCloudView, HeroSunfloweyView, HeroJesterView

urlpatterns = [
    url(r'^heroes$', HeroesView.as_view(), name='Heroes'),
    url(r'^hero/cloud$', HeroCloudView.as_view(), name='Cloud'),
    url(r'^hero/sunflowey$', HeroSunfloweyView.as_view(), name='Sunflowey'),
    url(r'^hero/jester$', HeroJesterView.as_view(), name='Jester'),
]
    
